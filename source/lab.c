#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct item {
int digit;
struct item *next;
struct item *prev;
} Item;

typedef struct mnumber {
Item *head;
Item *tail;
} Tnumber;

void AddDigit(Tnumber *number, int digit);
void PrintTnumber(Tnumber number);
void Subtract(Tnumber *x, Tnumber *y);
void Divide2(Tnumber *x);
int GSD(Tnumber x, Tnumber y);
int Compare(Tnumber x, Tnumber y);

int main(void)
{
Tnumber a = { NULL, NULL }, b = { NULL, NULL };
printf("enter a: ");
char c;
c = getchar();
while ((c > 47) && (c < 72)){
AddDigit(&a, c - '0');
c = getchar();
}
printf("enter b: ");
c = getchar();
while ((c > 47) && (c < 72)){
AddDigit(&b, c - '0');
c = getchar();
}

if (GSD(a, b) == 1)
printf("not prime");
else
printf("prime");

Item *p;
while (a.head) {
p = a.head;
a.head = a.head->next;
free(p);
}

while (b.head) {
p = b.head;
b.head = b.head->next;
free(p);
}
getchar();
return 0;
}

void AddDigit(Tnumber *number, int digit)
{
Item *p = (Item *)malloc(sizeof(Item));
p->digit = digit;
p->next = p->prev = NULL;
if (number->head == NULL)
number->head = number->tail = p;
else {
number->tail->next = p;
p->prev = number->tail;
number->tail = p;
}
}

void Subtract(Tnumber *x, Tnumber *y)
{
Item *px = x->tail, *py = y->tail;
int digit;
int pos = 0, s = py->digit;
while (px) {
digit = px->digit - s - pos;
if (digit < 0) {
digit += 10;
pos = 1;
}
else pos = 0;
px->digit = digit;
px = px->prev;
if (py->prev) {
py = py->prev;
s = py->digit;
}
else s = 0;
}
while (x->head->digit == 0) x->head = x->head->next;
}

void Divide2(Tnumber *x)
{
Item *px = x->head;
int digit, ost = 0;
while (px) {
digit = ost * 10 + px->digit;
ost = digit % 2;
digit /= 2;
px->digit = digit;
px = px->next;
}
while (x->head->digit == 0) x->head = x->head->next;
}

void PrintTnumber(Tnumber number)
{
Item *p = number.head;
printf("\nNumber: ");
while (p) {
printf("%d", p->digit);
p = p->next;
}
}

int Compare(Tnumber x, Tnumber y){
Item *px = x.head, *py = y.head;
int k = 0;

while (px&&py){
if (k == 0){
if (px->digit > py->digit) k = 1;
if (py->digit > px->digit) k = -1;
}
px = px->next;
py = py->next;
}
if ((!px) && (!py))return k;
if (!py) return 1;
if (!px) return -1;
return 0;
}

int GSD(Tnumber x, Tnumber y)
{
while (Compare(x, y) != 0) {

if ((x.tail->digit % 2 == 0) && (y.tail->digit % 2 == 0))
return 1;
while (x.tail->digit % 2 == 0)
Divide2(&x);
while (y.tail->digit % 2 == 0)
Divide2(&y);
if (Compare(x, y) == 0)
break;
else
if (Compare(x, y) == 1)
Subtract(&x, &y);
else
if (Compare(x, y) == -1)
Subtract(&y, &x);
}
if ((x.head == x.tail) && (x.head->digit == 1))
return 0;
else
return 1;
}
